#!/bin/sh
# stupid sync script to get the last pure and packets modules
umount /opt/pure
umount /opt/packets
rsync -P royalrabbit.goto10.org::puredyne/testing/modules/pure.dyne \
 $DYNE_SYS_MNT/modules/
rsync -P royalrabbit.goto10.org::puredyne/testing/modules/packets.dyne \
 $DYNE_SYS_MNT/modules/
mount -o loop $DYNE_SYS_MNT/modules/pure.dyne /opt/pure
mount -o loop $DYNE_SYS_MNT/modules/packets.dyne /opt/packets
ldconfig
