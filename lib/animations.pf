# animations.pf
# Here are defined the various extra animations that happen out of the main
# compute organisms cycle.

# JUMP ANIMATION

(
	jump_counter
	jump_position
	jump_color
	jump_shape
	jump_rot
	jump_lifespan
)

	context
	
	read-jumpanim
	write-jumpanim

0 	jump_counter 	!
0	jump_position 	!
0	jump_color	!
0	jump_shape	!
0	jump_rot	!
0	jump_lifespan	!

()	variable!	jump-population

: collect-jump-info
	my_lifespan @
	my_face_rotation @
	my_shape @
	my_shapecolor @
	my_position @
	1
	6 pack jump-population queue
	;

: jump
	jump-population pop read-jumpanim
	jump_counter @ dup
        0 > if
                m-push
			jump_position @ trans
			jump_rot @ 90. + rotz	
		
			jump_shape @ 1. jump_counter @ 
			>float 0.3 * + dup >r * 
		
			jump_color @ jump_lifespan @ 
			>float 300. / 1. 
		
			jump_counter @ 
			>float 10. / - * concat
		
			15 ndup 16 pack >matrix
                	2 pack "cv" trianglefan

			# outline
                	jump_shape @ 2 r> * *
			(1. 1. 1.) 0.11 1. 

			jump_counter @ 
			>float 10. / - * concat
		
			14 ndup 15 pack >matrix
                	2 pack "cv" lineloop		

			#1 1 1 1 rgba 0.1 square
                	1 + 10 mod jump_counter !
			write-jumpanim jump-population queue
		m-pop
	else
 		drop
        then
        ;
	

: jumpanim
	jump-population @ 
	size swap drop
	for jump next
	;

# DEATH ANIMATION

(
	death_counter
	death_position
	death_color
	death_shape
	death_rot
	death_lifespan
)

	context
	
	read-deathanim
	write-deathanim

0 	death_counter 	!
0	death_position 	!
0	death_color	!
0	death_shape	!
0	death_rot	!
0	death_lifespan	!

()	variable!	death-population

: collect-death-info
	my_lifespan @
	my_face_rotation @
	my_shape @
	my_shapecolor @
	my_position @
	1
	6 pack death-population queue
	;

: death
	death-population pop read-deathanim
        death_counter @ dup
        0 > if
		m-push
		death_position @ trans
		# black hole
		precise_disc 0.05 *
                (0. 0.1 .4 0.)
                36 ndup
                (0. .2 .1) .4 death_counter @
                >float 100. / - concat
                38 pack >matrix
                2 pack "cv" trianglefan	

		# cross
		cross2 0.05 *
		(1. 1. 1.) 0.5 
		death_counter @ 100. / 
		- concat
		3 ndup 4 pack >matrix
		2 pack "cv" lines

		# organism body zoom out
		death_rot @ 90. + rotz

                # outline
                death_shape @ 2. death_counter @ 
		>float 20. / - *
                
		(1. 1. 1.) 0.11 1. death_counter @ 
		>float 20. / - * concat
                
		14 ndup 15 pack >matrix
                2 pack "cv" lineloop

                m-pop
		
		1 + 50 mod death_counter !
                write-deathanim death-population queue

        else
                drop
        then
	;

: deathanim
        death-population @ size swap drop
        for death next
        ;


# ARRIVAL ANIMATION

(
        arrival_counter
        arrival_position
        arrival_color
        arrival_shape
        arrival_rot
)

        context

        read-arrivalanim
        write-arrivalanim

0       arrival_counter    !
0       arrival_position   !
0       arrival_color      !
0       arrival_shape      !
0       arrival_rot        !

()      variable!       arrival-population

: collect-arrival-info
        my_face_rotation @
        my_shape @
        my_shapecolor @
        my_position @
        1
        5 pack arrival-population queue
        ;

: arrival
        arrival-population pop read-arrivalanim
        arrival_counter @ dup
        
	0 > if
                m-push
			arrival_position @ trans			
			1 blend
			precise_disc 0.1 *
			(.9 .8 1. 0.)
                        36 ndup 
			(.8 .8 1.) .4 arrival_counter @
                        >float 100. / - concat
			38 pack >matrix
                        2 pack "cv" trianglefan
			2 blend
                m-pop
                1 + 50 mod arrival_counter !
                write-arrivalanim arrival-population queue
        else
                drop
        then
        ;


: arrivalanim
        arrival-population @ size swap drop
        for arrival next
        ;

# BIRTH ANIMATION
: birthanim
	1 blend
	m-push
        	10. my_speed @ - 200.
        	my_counter @ 2. * - / 0.04 +
        	scale
        	birth_mark
        m-pop
        2 blend
	;

# BIRTH ZOOM IN ANIMATION
: birthzoom
	# core
	my_shape @ my_counter @ 0.01 * *
	my_shapecolor @ 
	my_lifespan @ >float 0.033 * concat
	15 ndup
	16 pack 
        >matrix 
	2 pack "cv" trianglefan
	
	# outline
	my_shape @ my_counter @ 0.02 * *
	alpha_white2
        2 pack "cv" lineloop
	;


# FERTILITY ANIMATION
: fertanim
	1 blend
        my_shape @ 2 *
        my_shapecolor @
        my_counter @ 10 mod >float 0.025 * concat
        14 ndup (0. 0. 0. 0.)
        16 pack >matrix
        2 pack "cv" trianglefan
        2 blend
	;

# ORGANISM ANIMATION
: organim
	# core
	my_shape @
        my_shapecolor @
        my_lifespan @ >float 0.033 * concat
        15 ndup
        16 pack
        >matrix
	2 pack "cv" trianglefan

        # outline
        my_shape @ 2 * alpha_white2
        2 pack "cv" lineloop
	;

# FINGER ANIMATION
: fingeranim
	touched @
	1 = if	
		m-push
		finger-pos @ trans
		fingermark	
		m-pop
	then
	;
